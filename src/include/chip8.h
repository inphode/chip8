#ifndef CHIP8_H_
#define CHIP8_H_


#include <stdint.h>

#include <SDL.h>


// Memory location sizes
// Defined as preprocessor constants to allow usage in array initialisers
// Number of general purpose registers
#define CHIP8_SIZE_GP_REGISTERS (16)
// Internal display size
#define CHIP8_SIZE_WIDTH (64)
#define CHIP8_SIZE_HEIGHT (32)
// Size of graphics memory (64x32 pixels)
#define CHIP8_SIZE_GFX (CHIP8_SIZE_WIDTH * CHIP8_SIZE_HEIGHT)
// Number of stack locations
#define CHIP8_SIZE_STACK (16)
// Number of input keys
#define CHIP8_SIZE_KEYS (16)
// Total size of system memory
#define CHIP8_SIZE_MEMORY (4096)
// Size of memory area reserved for interpreter
#define CHIP8_SIZE_INTERPRETER (512)
// Size of fontset in memory
#define CHIP8_SIZE_FONTSET (80)
// Size of each character in fontset
#define CHIP8_SIZE_FONTCHAR (5)
// As described on Wikipedia: reserved for call stack, internal use, and other variables
// But this area is not used by this interpreter for the call stack
#define CHIP8_SIZE_INTERNAL (96)
// Size of memory reserved for display refresh
#define CHIP8_SIZE_DISPLAY (256)
// The maximum size a ROM can be before running into reserved areas of memory
#define CHIP8_SIZE_MAX_ROM (0xCA0)

// Fixed locations in memory
// Start of memory reserved for interpreter
static const uint16_t CHIP8_MEMORY_INTERPRETER = 0x0;
// Start of memory reserved for built in fontset
static const uint16_t CHIP8_MEMORY_FONTSET = 0x50;
// Start of memory reserved for internal use
static const uint16_t CHIP8_MEMORY_INTERNAL = 0xEA0;
// Start of memory reserved for display refresh
static const uint16_t CHIP8_MEMORY_DISPLAY = 0xF00;
// Start of memory where ROM is loaded
static const uint16_t CHIP8_MEMORY_ROM = 0x200;

// CHIP-8 default fontset
static const uint8_t chip8_fontset[CHIP8_SIZE_FONTSET] = { 
    0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
    0x20, 0x60, 0x20, 0x20, 0x70, // 1
    0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
    0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
    0x90, 0x90, 0xF0, 0x10, 0x10, // 4
    0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
    0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
    0xF0, 0x10, 0x20, 0x40, 0x40, // 7
    0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
    0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
    0xF0, 0x90, 0xF0, 0x90, 0x90, // A
    0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
    0xF0, 0x80, 0x80, 0x80, 0xF0, // C
    0xE0, 0x90, 0x90, 0x90, 0xE0, // D
    0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
    0xF0, 0x80, 0xF0, 0x80, 0x80  // F
};

// Customisable configuration
typedef struct chip8_config {
    // Scale of the display
    struct {
        int x;
        int y;
    } scale;
    // Colour for background
    struct {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    } background_colour;
    // Colour for foreground
    struct {
        uint8_t r;
        uint8_t g;
        uint8_t b;
    } foreground_colour;
    // Sprite wrapping
    struct {
        int x;
        int y;
    } sprite_wrap;
    // Cycles Per Second (CPU Hz)
    int cycles_per_second;
    // Ticks Per Second (Timer Hz)
    int ticks_per_second;
} chip8_config_t;

// Representation of CHIP-8 state
typedef struct chip8 {
    // Configuration of this CHIP-8 state
    chip8_config_t *config;
    // Total cycles elapsed
    uint32_t cycles;
    // Currently executing opcode
    uint16_t opcode;
    struct {
        // General purpose registers
        union {
            uint8_t v[CHIP8_SIZE_GP_REGISTERS];
            struct {
                uint8_t v0, v1, v2, v3, v4, v5, v6, v7, v8, v9, va, vb, vc, vd, ve;
                union {
                    // Final general purpose register doubles as carry flag
                    uint8_t vf, cf;
                };
            };
        };
        // Address register
        uint16_t i;
        // Program counter
        uint16_t pc;
        // Stack pointer
        uint16_t sp;
    } registers;
    struct {
        // Used for timing the events of games. Can be set and read.
        uint8_t delay;
        // Used for sound effects. When nonzero, beeping sound is made.
        uint8_t sound;
    } timers;
    /* System memory
     * +---------------+= 0xFFF (4095) End of Chip-8 RAM
     * |               |
     * |               |
     * |               |
     * |               |
     * |               |
     * | 0x200 to 0xFFF|
     * |     Chip-8    |
     * | Program / Data|
     * |     Space     |
     * |               |
     * |               |
     * |               |
     * +- - - - - - - -+= 0x600 (1536) Start of ETI 660 Chip-8 programs
     * |               |
     * |               |
     * |               |
     * +---------------+= 0x200 (512) Start of most Chip-8 programs
     * | 0x000 to 0x1FF|
     * | Reserved for  |
     * |  interpreter  |
     * +---------------+= 0x000 (0) Start of Chip-8 RAM
    */
    uint8_t memory[CHIP8_SIZE_MEMORY];
    // Black and white 64x32 graphics memory
    uint8_t gfx[CHIP8_SIZE_GFX];
    // If flag is set, screen needs updating
    int draw_flag;
    // Stack
    uint16_t stack[CHIP8_SIZE_STACK];
    // Key state
    union {
        uint8_t keys[CHIP8_SIZE_KEYS];
        struct {
            uint8_t k0, k1, k2, k3, k4, k5, k6, k7, k8, k9, ka, kb, kc, kd, ke, kf;
        } key_code;
    };
} chip8_t;

// Read the configuration from file
chip8_config_t *chip8_read_config(char *file_name);
// Initialise CHIP-8 state
chip8_t *chip8_initialise(chip8_config_t *config);
// Shutdown CHIP-8, freeing memory structures
// If save_config is true, it will not free the config structure
void chip8_shutdown(chip8_t *chip8, int save_config);
// Shutdown and reinitialise CHIP-8
chip8_t *chip8_reset(chip8_t *chip8);
// Load ROM into CHIP-8 memory
int chip8_load(chip8_t *chip8, const char *file_name);
// Handle keyup/keydown events
void chip8_handle_key(chip8_t *chip8, SDL_Keycode key_code, int value);
// Update the screen from graphics memory
void chip8_video_update(chip8_t *chip8, SDL_Renderer *renderer);
// Start the emulation
int chip8_run(chip8_t *chip8);


#endif /* CHIP8_H_ */
