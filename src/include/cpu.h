#ifndef CHIP8_CPU_H_
#define CHIP8_CPU_H_


#include "chip8.h"


void (*chip8_opcodes[17])(chip8_t *chip8);
void (*chip8_opcodes_arithmetic[16])(chip8_t *chip8);
void (*chip8_opcodes_memory[16])(chip8_t *chip8);

// Emulate one cycle of the CPU
int chip8_emulate_cycle(chip8_t *chip8, int cycles_per_tick);

// Opcodes CLS and RET
void chip8_cpu_cls_ret(chip8_t *chip8);
// Opcodes for arithmetic
void chip8_cpu_arithmetic(chip8_t *chip8);
// Opcodes SKP and SKNP
void chip8_cpu_skp_sknp(chip8_t *chip8);
// Opcodes for memory operations
void chip8_cpu_memory(chip8_t *chip8);

// Unknown opcode
void chip8_cpu_null(chip8_t *chip8);

// 00E0 - CLS
// Clear the display
void chip8_cpu_cls(chip8_t *chip8);

// 00EE - RET
// Return from a subroutine
void chip8_cpu_ret(chip8_t *chip8);

// 1nnn - JP addr
// Jump to location nnn
void chip8_cpu_jp_addr(chip8_t *chip8);

// 2nnn - CALL addr
// Call subroutine at nnn
void chip8_cpu_call_addr(chip8_t *chip8);

// 3xkk - SE Vx, byte
// Skip next instruction if Vx = kk
void chip8_cpu_se_vx_byte(chip8_t *chip8);

// 4xkk - SNE Vx, byte
// Skip next instruction if Vx != kk
void chip8_cpu_sne_vx_byte(chip8_t *chip8);

// 5xy0 - SE Vx, Vy
// Skip next instruction if Vx = Vy
void chip8_cpu_se_vx_vy(chip8_t *chip8);

// 6xkk - LD Vx, byte
// Set Vx = kk
void chip8_cpu_ld_vx_byte(chip8_t *chip8);

// 7xkk - ADD Vx, byte
// Set Vx = Vx + kk
void chip8_cpu_add_vx_byte(chip8_t *chip8);

// 8xy0 - LD Vx, Vy
// Set Vx = Vy
void chip8_cpu_ld_vx_vy(chip8_t *chip8);

// 8xy1 - OR Vx, Vy
// Set Vx = Vx OR Vy
void chip8_cpu_or_vx_vy(chip8_t *chip8);

// 8xy2 - AND Vx, Vy
// Set Vx = Vx AND Vy
void chip8_cpu_and_vx_vy(chip8_t *chip8);

// 8xy3 - XOR Vx, Vy
// Set Vx = Vx XOR Vy
void chip8_cpu_xor_vx_vy(chip8_t *chip8);

// 8xy4 - ADD Vx, Vy
// Set Vx = Vx + Vy, set VF = carry
void chip8_cpu_add_vx_vy(chip8_t *chip8);

// 8xy5 - SUB Vx, Vy
// Set Vx = Vx - Vy, set VF = NOT borrow
void chip8_cpu_sub_vx_vy(chip8_t *chip8);

// 8xy6 - SHR Vx {, Vy}
// Set Vx = Vx SHR 1
void chip8_cpu_shr_vx_vy(chip8_t *chip8);

// 8xy7 - SUBN Vx, Vy
// Set Vx = Vy - Vx, set VF = NOT borrow
void chip8_cpu_subn_vx_vy(chip8_t *chip8);

// 8xyE - SHL Vx {, Vy}
// Set Vx = Vx SHL 1
void chip8_cpu_shl_vx_vy(chip8_t *chip8);

// 9xy0 - SNE Vx, Vy
// Skip next instruction if Vx != Vy
void chip8_cpu_sne_vx_vy(chip8_t *chip8);

// Annn - LD I, addr
// Set I = nnn
void chip8_cpu_ld_i_addr(chip8_t *chip8);

// Bnnn - JP V0, addr
// Jump to location nnn + V0
void chip8_cpu_jp_v0_addr(chip8_t *chip8);

// Cxkk - RND Vx, byte
// Set Vx = random byte AND kk
void chip8_cpu_rnd_vx_byte(chip8_t *chip8);

// Dxyn - DRW Vx, Vy, nibble
// Display n-byte sprite starting at memory location I at (Vx, Vy), set VF = collision
void chip8_cpu_drw_vx_vy_nibble(chip8_t *chip8);

// Ex9E - SKP Vx
// Skip next instruction if key with the value of Vx is pressed
void chip8_cpu_skp_vx(chip8_t *chip8);

// ExA1 - SKNP Vx
// Skip next instruction if key with the value of Vx is not pressed
void chip8_cpu_sknp_vx(chip8_t *chip8);

// Fx07 - LD Vx, DT
// Set Vx = delay timer value
void chip8_cpu_ld_vx_dt(chip8_t *chip8);

// Fx0A - LD Vx, K
// Wait for a key press, store the value of the key in Vx
void chip8_cpu_ld_vx_key(chip8_t *chip8);

// Fx15 - LD DT, Vx
// Set delay timer = Vx
void chip8_cpu_ld_dt_vx(chip8_t *chip8);

// Fx18 - LD ST, Vx
// Set sound timer = Vx
void chip8_cpu_ld_st_vx(chip8_t *chip8);

// Fx1E - ADD I, Vx
// Set I = I + Vx
void chip8_cpu_add_i_vx(chip8_t *chip8);

// Fx29 - LD F, Vx
// Set I = location of sprite for digit Vx
void chip8_cpu_ld_f_vx(chip8_t *chip8);

// Fx33 - LD B, Vx
// Store BCD representation of Vx in memory locations I, I+1, and I+2
void chip8_cpu_ld_b_vx(chip8_t *chip8);

// Fx55 - LD [I], Vx
// Store registers V0 through Vx in memory starting at location I
void chip8_cpu_ld_i_vx(chip8_t *chip8);

// Fx65 - LD Vx, [I]
// Read registers V0 through Vx from memory starting at location I
void chip8_cpu_ld_vx_i(chip8_t *chip8);



#endif /* CHIP8_CPU_H_ */
