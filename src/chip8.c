#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include "minIni.h"

#include "chip8.h"
#include "cpu.h"


chip8_config_t *chip8_read_config(char *file_name) {
    // Allocate memory for new chip8 structure
    chip8_config_t *config = (chip8_config_t *)malloc(sizeof(chip8_config_t));

    config->cycles_per_second = ini_getl("Emulation", "CyclesPerSecond", 600, file_name);
    config->ticks_per_second = ini_getl("Emulation", "TicksPerSecond", 60, file_name);

    config->sprite_wrap.x = ini_getbool("Emulation", "SpriteWrapX", 1, file_name);
    config->sprite_wrap.y = ini_getbool("Emulation", "SpriteWrapY", 1, file_name);

    config->scale.x = ini_getl("Video", "ScaleX", 10, file_name);
    config->scale.y = ini_getl("Video", "ScaleY", 10, file_name);

    config->background_colour.r = ini_getl("Video", "BackgroundR", 0, file_name);
    config->background_colour.g = ini_getl("Video", "BackgroundG", 0, file_name);
    config->background_colour.b = ini_getl("Video", "BackgroundB", 100, file_name);

    config->foreground_colour.r = ini_getl("Video", "ForegroundR", 0, file_name);
    config->foreground_colour.g = ini_getl("Video", "ForegroundG", 200, file_name);
    config->foreground_colour.b = ini_getl("Video", "ForegroundB", 0, file_name);

    // Sanity check values, to avoid strange behavior and division by zero etc.
    if (config->cycles_per_second < 1) config->cycles_per_second = 1;
    if (config->ticks_per_second < 1) config->ticks_per_second = 1;
    if (config->ticks_per_second > config->cycles_per_second) config->ticks_per_second = config->cycles_per_second;

    if (config->scale.x < 1) config->scale.x = 1;
    if (config->scale.y < 1) config->scale.y = 1;

    if (config->background_colour.r > 255) config->background_colour.r = 255;
    if (config->background_colour.g > 255) config->background_colour.g = 255;
    if (config->background_colour.b > 255) config->background_colour.b = 255;

    if (config->background_colour.r < 0) config->background_colour.r = 0;
    if (config->background_colour.g < 0) config->background_colour.g = 0;
    if (config->background_colour.b < 0) config->background_colour.b = 0;

    if (config->foreground_colour.r > 255) config->foreground_colour.r = 255;
    if (config->foreground_colour.g > 255) config->foreground_colour.g = 255;
    if (config->foreground_colour.b > 255) config->foreground_colour.b = 255;

    if (config->foreground_colour.r < 0) config->foreground_colour.r = 0;
    if (config->foreground_colour.g < 0) config->foreground_colour.g = 0;
    if (config->foreground_colour.b < 0) config->foreground_colour.b = 0;

    return config;
}

chip8_t *chip8_initialise(chip8_config_t *config) {
    // Allocate memory for new chip8 structure
    chip8_t *chip8 = (chip8_t *)malloc(sizeof(chip8_t));

    // Number of cycles executed
    chip8->cycles = 0;
    // Clear current opcode
    chip8->opcode = 0;
    // Clear draw flag (set to one, to clear the screen at start)
    chip8->draw_flag = 1;
    // Clear general purpose registers
    memset(chip8->registers.v, 0, sizeof(uint8_t) * CHIP8_SIZE_GP_REGISTERS);
    // Clear other registers
    chip8->registers.i = 0;
    chip8->registers.sp = 0;
    // Set initial program counter to default program starting location
    chip8->registers.pc = CHIP8_MEMORY_ROM;
    // Clear timers
    chip8->timers.delay = 0;
    chip8->timers.sound = 0;
    // Clear system memory
    memset(chip8->memory, 0, sizeof(uint8_t) * CHIP8_SIZE_MEMORY);
    // Clear graphics memory
    memset(chip8->gfx, 0, sizeof(uint8_t) * CHIP8_SIZE_GFX);
    // Clear call stack
    memset(chip8->stack, 0, sizeof(uint16_t) * CHIP8_SIZE_STACK);
    // Clear keystate
    memset(chip8->keys, 0, sizeof(uint8_t) * CHIP8_SIZE_KEYS);
    // Load fontset
    memcpy(chip8->memory + CHIP8_MEMORY_FONTSET, chip8_fontset, sizeof(uint8_t) * CHIP8_SIZE_FONTSET);

    // Set configuration
    chip8->config = config;

    // Set RNG seed to current time in seconds
    srand((unsigned)time(NULL));

    return chip8;
}

void chip8_shutdown(chip8_t *chip8, int save_config) {
    if (!save_config) {
        free(chip8->config);
    }
    free(chip8);
}

chip8_t *chip8_reset(chip8_t *chip8) {
    // Save current config
    chip8_config_t *config = chip8->config;
    chip8_shutdown(chip8, 1);
    return chip8_initialise(config);
}

int chip8_load(chip8_t *chip8, const char *file_name) {
    // Open binary file for reading
    FILE *file = fopen(file_name, "rb");
    // Find the size of the file
    fseek(file, 0, SEEK_END); 
    size_t file_size = ftell(file);
    fseek(file, 0, SEEK_SET);
    // Check for empty file
    if (file_size == 0) {
        printf("Error: Supplied file was found to be empty\n");
        return -1;
    }
    // Check for ROM max size
    if (file_size > CHIP8_SIZE_MAX_ROM) {
        printf("Error: Supplied file too large to fit into CHIP-8 memory\n");
        return -1;
    }
    // Read file directly to CHIP-8 memory
    if (fread(chip8->memory + CHIP8_MEMORY_ROM, sizeof(chip8->memory[0]), file_size, file) != file_size) {
        printf("Error: There was a problem reading the file\n");
        return -1;
    }
    fclose(file);

    return 0;
}

void chip8_handle_key(chip8_t *chip8, SDL_Keycode key_code, int value) {
    switch (key_code) {
        // Map keys to CHIP-8 keypad
        case SDLK_x: chip8->key_code.k0 = value; break;
        case SDLK_1: chip8->key_code.k1 = value; break;
        case SDLK_2: chip8->key_code.k2 = value; break;
        case SDLK_3: chip8->key_code.k3 = value; break;
        case SDLK_q: chip8->key_code.k4 = value; break;
        case SDLK_w: chip8->key_code.k5 = value; break;
        case SDLK_e: chip8->key_code.k6 = value; break;
        case SDLK_a: chip8->key_code.k7 = value; break;
        case SDLK_s: chip8->key_code.k8 = value; break;
        case SDLK_d: chip8->key_code.k9 = value; break;
        case SDLK_z: chip8->key_code.ka = value; break;
        case SDLK_c: chip8->key_code.kb = value; break;
        case SDLK_4: chip8->key_code.kc = value; break;
        case SDLK_r: chip8->key_code.kd = value; break;
        case SDLK_f: chip8->key_code.ke = value; break;
        case SDLK_v: chip8->key_code.kf = value; break;
    }
}

void chip8_video_update(chip8_t *chip8, SDL_Renderer *renderer) {
    // Clear the screen
    SDL_SetRenderDrawColor(
        renderer,
        chip8->config->background_colour.r,
        chip8->config->background_colour.g,
        chip8->config->background_colour.b,
        255
    );
    SDL_RenderClear(renderer);

    // Draw to the screen from graphics memory
    SDL_SetRenderDrawColor(
        renderer,
        chip8->config->foreground_colour.r,
        chip8->config->foreground_colour.g,
        chip8->config->foreground_colour.b,
        255
    );
    // Counter to avoid having to calculate index
    int i = 0;
    // Rectangle used for drawing pixels as rectangles
    SDL_Rect rectangle;
    // The scale is constant here, so we set it before the loop
    rectangle.w = chip8->config->scale.x;
    rectangle.h = chip8->config->scale.y;
    // Loop through each row
    for (int y = 0; y < CHIP8_SIZE_HEIGHT; y++) {
        // Loop through each column
        for (int x = 0; x < CHIP8_SIZE_WIDTH; x++, i++) {
            // Check if pixel is set
            if (chip8->gfx[i]) {
                // Draw the pixel as a filled rectangle
                rectangle.x = x * chip8->config->scale.x;
                rectangle.y = y * chip8->config->scale.y;
                SDL_RenderFillRect(renderer, &rectangle);
            }
        }
    }

    // Flip the screen buffer
    SDL_RenderPresent(renderer);
}

int chip8_run(chip8_t *chip8) {
    // Create window and renderer for SDL2
    SDL_Window *window = SDL_CreateWindow(
        "C8 - CHIP-8 interpreter - v"VERSION_STRING,
        SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        CHIP8_SIZE_WIDTH * chip8->config->scale.x,
        CHIP8_SIZE_HEIGHT * chip8->config->scale.y,
        0 // Flags
    );
    SDL_Renderer *renderer = SDL_CreateRenderer(window, -1, 0);

    // Cycles Per Second timer
    Uint32 cps_start_ticks = SDL_GetTicks();
    // Ticks Per Frame
    int ticks_per_frame = 1000 / chip8->config->cycles_per_second;
    
    // The numbers of cycles for every timer clock tick
    int cycles_per_tick = chip8->config->cycles_per_second / chip8->config->ticks_per_second;

    // Is the emulation running
    int running = 1;
    // Should the emulation restart instead of exit
    int reset = 0;
    while (running) {
        SDL_Event event;
        while (SDL_PollEvent(&event)) {
            switch (event.type) {

            case SDL_KEYDOWN:
                chip8_handle_key(chip8, event.key.keysym.sym, 1);
                if (event.key.keysym.sym == SDLK_ESCAPE) {
                    running = 0;
                }
                if (event.key.keysym.sym == SDLK_BACKSPACE) {
                    running = 0;
                    reset = 1;
                }
                break;

            case SDL_KEYUP:
                chip8_handle_key(chip8, event.key.keysym.sym, 0);
                break;

            case SDL_QUIT:
                running = 0;
                break;
            }
        }

        // Cycle cap timer
        Uint32 cap_start_ticks = SDL_GetTicks();

        // Calculate and correct Cycles Per Second
        float average_cps = chip8->cycles / (SDL_GetTicks() - cps_start_ticks / 1000.f);
        if (average_cps > 2000000) {
            average_cps = 0;
        }

        // Emulate a single cycle of the CPU
        if (chip8_emulate_cycle(chip8, cycles_per_tick)) {
            // Fatal error occurred
            break;
        }

        // If last instruction changed graphics memory, we need to update screen
        if (chip8->draw_flag) {
            chip8_video_update(chip8, renderer);
        }

        // Increment cycle count
        chip8->cycles++;

        // If cycle finished early
        int cycle_ticks = SDL_GetTicks() - cap_start_ticks;
        if (cycle_ticks < ticks_per_frame) {
            // Wait remaining time
            SDL_Delay(ticks_per_frame - cycle_ticks);
        }
    }

    // Free memory from window and renderer
    SDL_DestroyWindow(window);
    SDL_DestroyRenderer(renderer);

    return reset;
}
