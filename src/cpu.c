#include <stdio.h>
#include <time.h>

#include "cpu.h"


void (*chip8_opcodes[17])(chip8_t *chip8) = 
{
    chip8_cpu_cls_ret, chip8_cpu_jp_addr, chip8_cpu_call_addr, chip8_cpu_se_vx_byte,
    chip8_cpu_sne_vx_byte, chip8_cpu_se_vx_vy, chip8_cpu_ld_vx_byte, chip8_cpu_add_vx_byte,
    chip8_cpu_arithmetic, chip8_cpu_sne_vx_vy, chip8_cpu_ld_i_addr, chip8_cpu_jp_v0_addr,
    chip8_cpu_rnd_vx_byte, chip8_cpu_drw_vx_vy_nibble, chip8_cpu_skp_sknp, chip8_cpu_memory,
    chip8_cpu_null
};

void (*chip8_opcodes_arithmetic[16])(chip8_t *chip8) = 
{
    chip8_cpu_ld_vx_vy, chip8_cpu_or_vx_vy, chip8_cpu_and_vx_vy, chip8_cpu_xor_vx_vy,
    chip8_cpu_add_vx_vy, chip8_cpu_sub_vx_vy, chip8_cpu_shr_vx_vy, chip8_cpu_subn_vx_vy,
    chip8_cpu_null, chip8_cpu_null, chip8_cpu_null, chip8_cpu_null, chip8_cpu_null,
    chip8_cpu_null, chip8_cpu_shl_vx_vy, chip8_cpu_null
};

void (*chip8_opcodes_memory[16])(chip8_t *chip8) = 
{
    chip8_cpu_null, chip8_cpu_null, chip8_cpu_null, chip8_cpu_ld_b_vx, chip8_cpu_null,
    chip8_cpu_null, chip8_cpu_null, chip8_cpu_ld_vx_dt, chip8_cpu_ld_st_vx, chip8_cpu_ld_f_vx,
    chip8_cpu_ld_vx_key, chip8_cpu_null, chip8_cpu_null, chip8_cpu_null, chip8_cpu_add_i_vx,
    chip8_cpu_null
};


int chip8_emulate_cycle(chip8_t *chip8, int cycles_per_tick) {
    // Fetch next opcode
    chip8->opcode = chip8->memory[chip8->registers.pc];
    chip8->opcode <<= 8;
    chip8->opcode |= chip8->memory[chip8->registers.pc + 1];
    
    // Go to next instruction
    chip8->registers.pc += 2;

    // Execute opcode from function pointer table
    printf("> [CYCLES: 0x%04X] [PC: 0x%04X] [OPCODE: 0x%04X] ", chip8->cycles, chip8->registers.pc - 2, chip8->opcode);
    chip8_opcodes[(chip8->opcode & 0xF000) >> 12](chip8);

    // Only run change the timers according to TicksPerSecond config
    if (chip8->cycles % cycles_per_tick == 0) {
        // Update timers
        if (chip8->timers.delay > 0) {
            chip8->timers.delay--;
        }
        if (chip8->timers.sound > 0) {
            if (chip8->timers.sound > 1) {
                // TODO: Try using \a or \7 to produce a beep from the console
                printf("> SOUND (BEEP!)\n");
            }
            chip8->timers.sound--;
        }
    }

    return 0;
}

void chip8_cpu_cls_ret(chip8_t *chip8) {
    if ((chip8->opcode & 0x00FF) == 0x00E0) {
        chip8_cpu_cls(chip8);
    } else {
        chip8_cpu_ret(chip8);
    }
}

void chip8_cpu_arithmetic(chip8_t *chip8) {
    chip8_opcodes_arithmetic[(chip8->opcode & 0x000F)](chip8);
}

void chip8_cpu_skp_sknp(chip8_t *chip8) {
    if ((chip8->opcode & 0x00FF) == 0x009E) {
        chip8_cpu_skp_vx(chip8);
    } else {
        chip8_cpu_sknp_vx(chip8);
    }
}

void chip8_cpu_memory(chip8_t *chip8) {
    if ((chip8->opcode & 0x000F) == 0x0005) {
        switch (chip8->opcode & 0x00F0) {
            case 0x0010: chip8_cpu_ld_dt_vx(chip8); break;
            case 0x0050: chip8_cpu_ld_i_vx(chip8); break;
            case 0x0060: chip8_cpu_ld_vx_i(chip8); break;
            default: chip8_cpu_null(chip8);
        }
    } else {
        chip8_opcodes_memory[(chip8->opcode & 0x000F)](chip8);
    }
}

void chip8_cpu_null(chip8_t *chip8) {
    // Reaching here would indicate a problem with the ROM code
    printf("NOP\n");
}

void chip8_cpu_cls(chip8_t *chip8) {
    printf("00E0 - CLS\n");
    // Clear graphics memory
    memset(chip8->gfx, 0, sizeof(uint8_t) * CHIP8_SIZE_GFX);
    chip8->draw_flag = 1;
}

void chip8_cpu_ret(chip8_t *chip8) {
    printf("00EE - RET\n");
    chip8->registers.pc = chip8->stack[chip8->registers.sp];
    chip8->registers.sp--;
}

void chip8_cpu_jp_addr(chip8_t *chip8) {
    printf("1nnn - JP addr\n");
    chip8->registers.pc = chip8->opcode & 0x0FFF;
}

void chip8_cpu_call_addr(chip8_t *chip8) {
    printf("2nnn - CALL addr\n");
    chip8->registers.sp++;
    chip8->stack[chip8->registers.sp] = chip8->registers.pc;
    chip8->registers.pc = chip8->opcode & 0x0FFF;
}

void chip8_cpu_se_vx_byte(chip8_t *chip8) {
    printf("3xkk - SE Vx, byte\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    if (chip8->registers.v[reg] == (chip8->opcode & 0x00FF)) {
        chip8->registers.pc += 2;
    }
}

void chip8_cpu_sne_vx_byte(chip8_t *chip8) {
    printf("4xkk - SNE Vx, byte\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    if (chip8->registers.v[reg] != (chip8->opcode & 0x00FF)) {
        chip8->registers.pc += 2;
    }
}

void chip8_cpu_se_vx_vy(chip8_t *chip8) {
    printf("5xy0 - SE Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    if (chip8->registers.v[reg1] == chip8->registers.v[reg2]) {
        chip8->registers.pc += 2;
    }
}

void chip8_cpu_ld_vx_byte(chip8_t *chip8) {
    printf("6xkk - LD Vx, byte\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->registers.v[reg] = chip8->opcode & 0x00FF;
}

void chip8_cpu_add_vx_byte(chip8_t *chip8) {
    printf("7xkk - ADD Vx, byte\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->registers.v[reg] += chip8->opcode & 0x00FF;
}

void chip8_cpu_ld_vx_vy(chip8_t *chip8) {
    printf("8xy0 - LD Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    chip8->registers.v[reg1] = chip8->registers.v[reg2];
}

void chip8_cpu_or_vx_vy(chip8_t *chip8) {
    printf("8xy1 - OR Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    chip8->registers.v[reg1] = chip8->registers.v[reg1] | chip8->registers.v[reg2];
}

void chip8_cpu_and_vx_vy(chip8_t *chip8) {
    printf("8xy2 - AND Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    chip8->registers.v[reg1] = chip8->registers.v[reg1] & chip8->registers.v[reg2];
}

void chip8_cpu_xor_vx_vy(chip8_t *chip8) {
    printf("8xy3 - XOR Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    chip8->registers.v[reg1] = chip8->registers.v[reg1] ^ chip8->registers.v[reg2];
}

void chip8_cpu_add_vx_vy(chip8_t *chip8) {
    printf("8xy4 - ADD Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    uint16_t result = chip8->registers.v[reg1] + chip8->registers.v[reg2];
    if (result > 255) {
        chip8->registers.cf = 1;
    } else {
        chip8->registers.cf = 0;
    }
    chip8->registers.v[reg1] = result & 0x00FF;
}

void chip8_cpu_sub_vx_vy(chip8_t *chip8) {
    printf("8xy5 - SUB Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    if (chip8->registers.v[reg1] > chip8->registers.v[reg2]) {
        chip8->registers.cf = 1;
    } else {
        chip8->registers.cf = 0;
    }
    // TODO: Check if this need to be 0 if Vx < Vy
    chip8->registers.v[reg1] = chip8->registers.v[reg1] - chip8->registers.v[reg2];
}

void chip8_cpu_shr_vx_vy(chip8_t *chip8) {
    printf("8xy6 - SHR Vx {, Vy}\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    if (chip8->registers.v[reg] & 0x01) {
        chip8->registers.cf = 1;
    } else {
        chip8->registers.cf = 0;
    }
    chip8->registers.v[reg] = chip8->registers.v[reg] >> 1;
}

void chip8_cpu_subn_vx_vy(chip8_t *chip8) {
    printf("8xy7 - SUBN Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    if (chip8->registers.v[reg2] > chip8->registers.v[reg1]) {
        chip8->registers.cf = 1;
    } else {
        chip8->registers.cf = 0;
    }
    // TODO: Check if this need to be 0 if Vx < Vy
    chip8->registers.v[reg1] = chip8->registers.v[reg2] - chip8->registers.v[reg1];
}

void chip8_cpu_shl_vx_vy(chip8_t *chip8) {
    printf("8xyE - SHL Vx {, Vy}\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    if (chip8->registers.v[reg] & 0x80) {
        chip8->registers.cf = 1;
    } else {
        chip8->registers.cf = 0;
    }
    chip8->registers.v[reg] = chip8->registers.v[reg] << 1;
}

void chip8_cpu_sne_vx_vy(chip8_t *chip8) {
    printf("9xy0 - SNE Vx, Vy\n");
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    if (chip8->registers.v[reg2] != chip8->registers.v[reg1]) {
        chip8->registers.pc += 2;
    }
}

void chip8_cpu_ld_i_addr(chip8_t *chip8) {
    printf("Annn - LD I, addr\n");
    chip8->registers.i = chip8->opcode & 0x0FFF;
}

void chip8_cpu_jp_v0_addr(chip8_t *chip8) {
    printf("Bnnn - JP V0, addr\n");
    chip8->registers.pc = (chip8->opcode & 0x0FFF) + chip8->registers.v0;
}

void chip8_cpu_rnd_vx_byte(chip8_t *chip8) {
    printf("Cxkk - RND Vx, byte\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    int random = rand() % 255;
    chip8->registers.v[reg] = (chip8->opcode & 0x00FF) & random;
}

void chip8_cpu_drw_vx_vy_nibble(chip8_t *chip8) {
    printf("Dxyn - DRW Vx, Vy, nibble\n");
    // Vx register contains draw location X coordinate
    uint8_t reg1 = (chip8->opcode & 0x0F00) >> 8;
    uint8_t x = chip8->registers.v[reg1];
    // Vy register contains draw location Y coordinate
    uint8_t reg2 = (chip8->opcode & 0x00F0) >> 4;
    uint8_t y = chip8->registers.v[reg2];
    // Final nibble contains number of rows in sprite
    int rows = chip8->opcode & 0x000F;
    // Carry flag should not be set unless collision detected
    chip8->registers.cf = 0;
    // Loop through each row in the sprite
    for (int row = 0; row < rows; row++) {
        // Get the sprite data for this row
        uint8_t sprite = chip8->memory[chip8->registers.i + row];
        // Loop through each column in the sprite
        for (int column = 0; column < 8; column++) {
            // We are not interested if the source sprite pixel is not set
            if ((sprite & (0x80 >> column)) == 0) {
                // Skip this column
                continue;
            }
            // Wrap to other side of screen if greater than screen width
            uint8_t target_x = x + column;
            if (target_x >= CHIP8_SIZE_WIDTH) {
                // Only wrap/draw pixel if wrapping enabled
                if (chip8->config->sprite_wrap.x) {
                    target_x -= CHIP8_SIZE_WIDTH;
                } else {
                    continue;
                }
            }
            // Wrap to other side of screen if greater than screen height
            uint8_t target_y = y + row;
            if (target_y >= CHIP8_SIZE_HEIGHT) {
                // Only wrap/draw pixel if wrapping enabled
                if (chip8->config->sprite_wrap.x) {
                    target_y -= CHIP8_SIZE_HEIGHT;
                } else {
                    continue;
                }
            }
            // Calculate target pixel
            int target = target_x + (target_y * CHIP8_SIZE_WIDTH);
            // Detect collision by checking if the target pixel is set
            if (chip8->gfx[target] == 1) {
                chip8->registers.cf = 1;
            }
            // Toggle pixel
            chip8->gfx[target] ^= 1;
        }
    }
    // Indicate that the screen needs redrawing
    chip8->draw_flag = 1;
}

void chip8_cpu_skp_vx(chip8_t *chip8) {
    printf("Ex9E - SKP Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    uint8_t key = chip8->registers.v[reg];
    if (chip8->keys[key]) {
        chip8->registers.pc += 2;
    }
}

void chip8_cpu_sknp_vx(chip8_t *chip8) {
    printf("ExA1 - SKNP Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    uint8_t key = chip8->registers.v[reg];
    if (!chip8->keys[key]) {
        chip8->registers.pc += 2;
    }
}

void chip8_cpu_ld_vx_dt(chip8_t *chip8) {
    printf("Fx07 - LD Vx, DT\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->registers.v[reg] = chip8->timers.delay;
}

void chip8_cpu_ld_vx_key(chip8_t *chip8) {
    printf("Fx0A - LD Vx, K\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    for (int i = 0; i < 16; i++) {
        if (chip8->keys[i]) {
            chip8->registers.v[reg] = i;
            return;
        }
    }
    // No key was found to be pressed, so make sure this instruction repeats
    chip8->registers.pc -= 2;
}

void chip8_cpu_ld_dt_vx(chip8_t *chip8) {
    printf("Fx15 - LD DT, Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->timers.delay = chip8->registers.v[reg];
}

void chip8_cpu_ld_st_vx(chip8_t *chip8) {
    printf("Fx18 - LD ST, Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->timers.sound = chip8->registers.v[reg];
}

void chip8_cpu_add_i_vx(chip8_t *chip8) {
    printf("Fx1E - ADD I, Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->registers.i += chip8->registers.v[reg];
}

void chip8_cpu_ld_f_vx(chip8_t *chip8) {
    printf("Fx29 - LD F, Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    chip8->registers.i = CHIP8_MEMORY_FONTSET + (chip8->registers.v[reg] * CHIP8_SIZE_FONTCHAR);
}

void chip8_cpu_ld_b_vx(chip8_t *chip8) {
    printf("Fx33 - LD B, Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    uint8_t value = chip8->registers.v[reg];
    chip8->memory[chip8->registers.i] = value / 100;
    chip8->memory[chip8->registers.i + 1] = (value / 10) % 10;
    chip8->memory[chip8->registers.i + 2] = (value % 100) % 10;
}

void chip8_cpu_ld_i_vx(chip8_t *chip8) {
    printf("Fx55 - LD [I], Vx\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    for (int i = 0; i < reg; i++) {
        chip8->memory[chip8->registers.i + i] = chip8->registers.v[i];
    }
}

void chip8_cpu_ld_vx_i(chip8_t *chip8) {
    printf("Fx65 - LD Vx, [I]\n");
    uint8_t reg = (chip8->opcode & 0x0F00) >> 8;
    for (int i = 0; i < reg; i++) {
        chip8->registers.v[i] = chip8->memory[chip8->registers.i + i];
    }
}


