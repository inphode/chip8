#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <stdint.h>
#include <ctype.h>

#include "chip8.h"


chip8_t *init(int argc, char **argv) {
    // Parse arguments for ROM file name
    if (argc != 2 && argc != 3) {
        printf("Error: Please supply a CHIP-8 ROM file: %s ROM [CONFIG]\n", argv[0]);
        exit(EXIT_FAILURE);
    }
    // Check read access to ROM file
    if (access(argv[1], R_OK) == -1) {
        printf("Error: Could not find '%s' or user has no read access\n", argv[1]);
        exit(EXIT_FAILURE);
    }
    // Parse arguments for config file name
    char *config_file;
    if (argc == 3) {
        config_file = argv[2];
    } else {
        config_file = "config.ini";
    }
    // Check read access to config file
    if (access(config_file, R_OK) == -1) {
        // Also check in config directory
        char str_buffer[256];
        snprintf(str_buffer, sizeof str_buffer, "%s%s", "config/", config_file);
        if (access(str_buffer, R_OK) == -1) {
            printf("Error: Could not find '%s' or user has no read access\n", config_file);
            exit(EXIT_FAILURE);
        } else {
            config_file = str_buffer;
        }
    }

    // Initialize SDL
    if (SDL_Init(SDL_INIT_TIMER | SDL_INIT_VIDEO | SDL_INIT_NOPARACHUTE) < 0) {
        printf("Error: SDL failed to initialise: %s\n", SDL_GetError());
        exit(EXIT_FAILURE);
    }
    atexit(SDL_Quit);

    // Get configuration from file
    chip8_config_t *config = chip8_read_config(config_file);

    // Get chip8 in initial state
    chip8_t *chip8 = chip8_initialise(config);
    printf("CHIP-8 system initialised\n");

    return chip8;
}

int main(int argc, char **argv) {
    // Disable STDOUT buffering
    setbuf(stdout, NULL);
    
    // Initialise SDL and CHIP-8
    chip8_t *chip8 = init(argc, argv);

    int running = 1;
    while (running) {
        // Load ROM into CHIP-8 memory
        printf("Loading ROM...\n");
        if (chip8_load(chip8, argv[1]) == -1) {
            printf("Error: Problem with ROM load operation\n");
            return EXIT_FAILURE;
        }
        printf("Loaded ROM '%s' successfully\n", argv[1]);

        // Start emulation
        int reset = chip8_run(chip8);

        // Check if reset was requested
        if (reset) {
            chip8 = chip8_reset(chip8);
        } else {
            running = 0;
        }
    }

    // Free memory
    chip8_shutdown(chip8, 0);

    return EXIT_SUCCESS;
}
