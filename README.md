# C8

A CHIP-8 emulator written in C using the C11 standard

## License and copyright

Programmed by Inphode (inphode /AT/ gmail /DOT/ com)

Released under the Revised BSD License. See LICENSE.txt for copyright and license.

## Compiling C8

### Ubuntu

1. Install dependencies: `sudo apt-get install build-essential libsdl2-dev`
2. Run `make` from the project root
3. You can find the output at `dist/c8`

### Windows

The below instructions assume a 64bit system.

1. Install MSYS2 from: [https://msys2.github.io](https://msys2.github.io) and follow all instructions on that page
2. Launch `msys2_shell.bat`, by default found in `C:\msys64`
3. Install a MinGW toolchain: `pacman -S mingw-w64-x86_64-toolchain`
4. Install SDL2: `pacman -S mingw-w64-x86_64-SDL2`
5. Launch `mingw64_shell.bat`, in `C:\msys64` by default
6. Run `make` from the project root
7. You can find the output at `dist/c8.exe`

#### Notes about MSYS2 usage

Always install packages with pacman from `msys2_shell.bat`

Always build C8 from `mingw64_shell.bat` otherwise extra dependencies (`msys-2.0.dll`) would be needed

You will need to put SDL2.dll in the `dist` folder to run `c8.exe` outside of the MinGW Shell

### Other operating systems

Currently the only dependency is SDL2, which is available and easy to install on a huge number of systems ([https://www.libsdl.org/](https://www.libsdl.org/)).

Simply run `make` from the project root and pkg-config will locate your SDL2 installation.

You can find the output at `dist/c8`.

## C8 configuration

C8 has a configuration file (by default called `config.ini`), which can be used to make customisations to the emulator. Such settings include the background and foreground colour of draw operations, emulation speed and scale of drawn pixels.

For documentation on what values can be set in the configuration file, see the comments in the sample config included in this repository.

## C8 usage

`c8` expects 1 argument, with an optional 2nd argument:

**Argument 1:** Path to ROM file (there are samples in the `roms` directory)

**Argument 2 (optional):** Configuration file (default `config.ini`)

### Notes

If the config file cannot be found in the current working directory, it will search in a directory named `config`

### Example

`./dist/c8 roms/TETRIS myconfig.ini`

### Keyboard controls

- Press `Esc` to exit
- Press `Backspace` to reset the emulation

The keyboard maps to the CHIP-8 keypad, as shown below:

```
1234     123C
QWER --> 456D
ASDF --> 789E
ZXCV     A0BF
```

## TODO

- Write proper and consistent error checking and reporting
- Create a multi-level logging system
- Improve the command line argument parsing
- Develop a cross-platform plugin system (This may be a step too far!)
- Attempt to find potential exploits, such as buffer overflows
- Experiment with how different optimisations affect the assembly output
- Detect invalid opcodes more thoroughly and prevent or catch crashes
- Handle custom key maps
- Error checking for config reader
- State saving and loading
- Implement proper timing, by separating cycles per second from CHIP-8 timers
- Add a debugger with breakpoints and more detailed output