# C Makefile for CHIP-8 emulator


# Configuration
include build/vars.mk
include build/arch.mk


# Default make rule
all: $(SOURCES) $(DISTDIR)/$(OUTPUT)

# Create final output (link)
$(DISTDIR)/$(OUTPUT): $(OBJS)
	$(CC) $(OBJS) -o $@ $(LDFLAGS)

# Create object files (compile)
$(OBJDIR)/%.o: $(SRCDIR)/%.c
	$(CC) $(CFLAGS) $< -o $@

# Directory creation
$(OBJS): | $(OBJDIR)
$(OBJDIR):
	@mkdir $(OBJDIR)
$(DISTDIR)/$(OUTPUT): | $(DISTDIR)
$(DISTDIR):
	@mkdir $(DISTDIR)

# Cleanup intermediate files
clean:
	$(RM) $(OBJDIR)/*.o

# Cleanup all generated files
distclean:
	$(RM) $(OBJDIR)
	$(RM) $(DISTDIR)
