# Configurable variables

# Version which will appear in window title
VERSION=0.0.1

# Standard utilities
RM=rm -rf
MKDIR=mkdir -p
CHMOD=chmod

# Directories
SRCDIR=src
INCDIR=$(SRCDIR)/include
OBJDIR=build/obj
DISTDIR=dist

# List of all sources to be compiled
SOURCES=$(notdir $(wildcard $(SRCDIR)/*.c))

# Compilers, assemblers, linkers
CC=gcc
LD=gcc
CFLAGS=-c -g -Wall -std=c11 -D VERSION_STRING=\"$(VERSION)\" -pedantic -I $(INCDIR) $(shell pkg-config sdl2 --cflags)
LDFLAGS=-Wall -g $(shell pkg-config sdl2 --libs)

OBJS=$(addprefix $(OBJDIR)/,$(SOURCES:.c=.o))
OUTPUT=c8
VPATH=src
